# GCP Ingress Firewall Module

This Terraform module is designed to be used for creating a GCP ingress firewall.

Due to opinionated security reasons, this module will not allow you to create an ingress rule that has allow rules with a `0.0.0.0/0` source range. You should use a Google Cloud Load Balancer, Identity Aware Proxy, a bastion host, or other proxy services to safely allow traffic from Internet (`0.0.0.0/0`). You can still create deny rules with a `0.0.0.0/0` source range.

[[_TOC_]]

### New Project Example

This example includes a basic configuration to get you started with using this module.
* [examples/new-project/main.tf](examples/new-project/main.tf)
* [examples/new-project/outputs.tf](examples/new-project/outputs.tf)
* [examples/new-project/variables.tf](examples/new-project/variables.tf)

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0 |
| <a name="requirement_google"></a> [google](#requirement\_google) | >= 4.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | >= 4.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [google_compute_firewall.ingress_compute_firewall](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_firewall) | resource |

## Inputs

| Name | Description | Type | Default | Required | Example Value |
|------|-------------|------|---------|:--------:|---------------|
| <a name="input_firewall_description"></a> [firewall\_description](#input\_firewall\_description) | Firewall rule description. This is a human friendly name that appears in the Cloud Console UI for administrator reference. | `string` | n/a | yes | <pre>"Allow traffic from home office IP"</pre> |
| <a name="input_firewall_disabled"></a> [firewall\_disabled](#input\_firewall\_disabled) | Enables or disables the firewall rule. This is usually only used when bypassing/overruling a firewall rule temporarily or permanently without removing the configuration. | `bool` | `false` | no | <pre>false</pre> |
| <a name="input_firewall_name"></a> [firewall\_name](#input\_firewall\_name) | Firewall rule name (Example: gitlab\_omnibus\_firewall\_rule) | `string` | n/a | yes | <pre>"dmurphy-home-office-rule"</pre> |
| <a name="input_firewall_priority"></a> [firewall\_priority](#input\_firewall\_priority) | The firewall rule priority relative to other firewall rules. | `number` | `1000` | no | <pre>1000</pre> |
| <a name="input_gcp_network"></a> [gcp\_network](#input\_gcp\_network) | The name or self link of the VPC network created in the parent module or environment. (Example: google\_compute\_network.vpc\_network.self\_link) | `string` | n/a | yes | <pre>"default"</pre> |
| <a name="input_gcp_project"></a> [gcp\_project](#input\_gcp\_project) | The GCP project ID (may be a alphanumeric slug) that the resources are deployed in. (Example: my-project-name) | `string` | n/a | yes | <pre> "example-project-id"</pre> |
| <a name="input_logging_enabled"></a> [logging\_enabled](#input\_logging\_enabled) | Enables logging to StackDriver | `bool` | `true` | no | <pre>true</pre> |
| <a name="input_rules_allow"></a> [rules\_allow](#input\_rules\_allow) | Firewall protocol(s) and port(s) to allow | <pre>list(object({<br>    protocol = string<br>    ports    = list(string)<br>  }))</pre> | `[]` | no | <pre>[<br/>  {<br/>    protocol = "tcp"<br/>    ports = ["22", "443"]<br/>  }<br/>]</pre> |
| <a name="input_rules_deny"></a> [rules\_deny](#input\_rules\_deny) | Firewall protocol(s) and port(s) to deny | <pre>list(object({<br>    protocol = string<br>    ports    = list(string)<br>  }))</pre> | `[]` | no | <pre>[<br/>  {<br/>    protocol = "all"<br/>    ports    = ["0-65535"]<br/>  }<br/>]</pre> |
| <a name="input_source_ranges"></a> [source\_ranges](#input\_source\_ranges) | List of public or internal IP CIDR ranges that traffic is coming from that this firewall rule should apply to. Either `source_ranges` or `source_tags` must have a value. For security reasons, this module is opinionated and prevents allowing 0.0.0.0/0 as a source range. | `list(string)` | `null` | no | <pre>["1.2.3.4/32"]</pre>
| <a name="input_source_tags"></a> [source\_tags](#input\_source\_tags) | List of tags applied to Compute Engine resources that traffic is coming from that this firewall rule should apply to. Either `source_ranges` or `source_tags` must have a value. | `list(string)` | `null` | no | <pre>["source_tag_1", "source_tag_2"]</pre> |
| <a name="input_target_tags"></a> [target\_tags](#input\_target\_tags) | List of tags applied to Compute Engine resources that this rule will be automatically associated with. | `list(string)` | n/a | yes | <pre>["gitlab-omnibus"]</pre> |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_firewall"></a> [firewall](#output\_firewall) | Firewall rule configuration |
| <a name="output_logging"></a> [logging](#output\_logging) | GCP Logging configuration |
| <a name="output_network"></a> [network](#output\_network) | GCP Network configuration |
| <a name="output_rules"></a> [rules](#output\_rules) | Allow and deny rules for ports and protocols |
| <a name="output_source"></a> [source](#output\_source) | Source that traffic is coming from that this firewall rule should apply to |
| <a name="output_target"></a> [target](#output\_target) | Destination resources that this firewall rule should apply to |
<!-- END_TF_DOCS -->

### Accessing Outputs

The outputs are returned as a map with sub arrays that use dot notation. You can see how each output is defined in [outputs.tf](outputs.tf).

```hcl
# Get a map with all values for the module
module.{{name}}

# Get a map with all values for the respective configuration
module.{{name}}.firewall
module.{{name}}.logging
module.{{name}}.network
module.{{name}}.rules
module.{{name}}.source
module.{{name}}.target

# Get individual values
module.{{name}}.firewall.description
module.{{name}}.firewall.direction
module.{{name}}.firewall.disabled
module.{{name}}.firewall.id
module.{{name}}.firewall.name
module.{{name}}.firewall.priority
module.{{name}}.firewall.self_link
module.{{name}}.logging.enabled
module.{{name}}.network.self_link
module.{{name}}.rules.allow
module.{{name}}.rules.deny
module.{{name}}.source.ranges
module.{{name}}.source.tags
module.{{name}}.target.tags
```

## Authors and Maintainers

* Dillon Wheeler / @dillonwheeler / dwheeler@gitlab.com
* Jeff Martin / @jeffersonmartin / jmartin@gitlab.com
